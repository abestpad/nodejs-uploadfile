var express = require("express");
var multer = require("multer");
var upload = multer({
  storage: multer.diskStorage({
    destination: (req, file, next) => {
      const folder = "./images/";
      if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
      }
      next(null, folder);
    },
    filename: function (req, file, next) {
      const ext = file.mimetype.split("/")[1];
      next(null, `${file.fieldname}-${Date.now()}.${ext}`);
    },
  }),
  limits: {
    fieldSize: 1024 * 1024 * 5,
  },
  fileFilter(req, file, next) {
    const image = file.mimetype.startsWith("image/");
    if (image) {
      next(null, true);
    } else [next({ message: "File type not supported" }, false)];
  },
});
var fs = require("fs");
var app = express();

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});
app.post("/upload", upload.single("file"), function (req, res) {
  //   res.send(req.files);
  res.json(req.files);
});
app.listen(5555, function () {
  console.log("App running on port 5555");
});
